﻿using System;

namespace DI_Resolver_DA_Layer
{
    public class DAObject : IDAObject
    {
        public bool Add()
        {
            Console.WriteLine("DataObject:Add");
            return true;
        }
    }
}
