# DI Resolver Pattern
The Dependency Injection Resolver Pattern is a refinement on a top down Dependency Injection IoC approach.

In a traditional top down DI/IoC solution dependencies are injected by the top level code. Unfortunately if your stack is more than two levels deep this means having to inject dependencies that are consumed by the lower levels in this top layer. This can lead to bloated constructors and messy code. Nobody likes messy code.

The DI Resolver Pattern allows you to encapsulate your Dependency Injection into a single 'Resolver' library and load all your dependencies in a single call to that library when you instantiate the DI kernel. This provides a clean abstraction, no dependencies to any lower levels (references or implied) and a single point of responsibility for dependency inclusion.

The code is derived from  a blog post by Panos @ 
http://codiply.com/blog/dependency-injection-across-multiple-layers-with-ninject

I'd like to thank Panos for his input
