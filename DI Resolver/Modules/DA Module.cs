﻿using DI_Resolver_DA_Layer;
using Ninject.Modules;

namespace DI_Resolver.DependencyResolver
{
    public class DA_Module : NinjectModule
    {
        public override void Load()
        {
            Bind<IDAObject>().To<DAObject>();
        }
    }
}