﻿using DI_Resolver_Business_Layer;
using Ninject.Modules;

namespace DI_Resolver.DependencyResolver
{
    public class BL_Module : NinjectModule
    {
        public override void Load()
        {
            Bind<IBusinessObject>().To<BusinessObject>();
        }
    }
}
