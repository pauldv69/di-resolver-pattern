﻿using System.Collections.Generic;
using DI_Resolver.DependencyResolver;
using Ninject;
using Ninject.Modules;

namespace DI_Resolver
{
    public class Resolver
    {
        public static void Load(IKernel kernel)
        {
            var modules = new List<INinjectModule>()
            {
                new BL_Module(),
                new DA_Module()
            };
            kernel.Load(modules);

        }
    }
}
