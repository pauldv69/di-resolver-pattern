﻿using System;
using DI_Resolver_DA_Layer;

namespace DI_Resolver_Business_Layer
{
    public class BusinessObject : IBusinessObject
    {
        private readonly IDAObject dataObject;

        public BusinessObject(IDAObject dataObject)
        {
            Console.WriteLine("BusinessObject Constructor");
            this.dataObject = dataObject;
        }

        public string StartProcessing(int id)
        {
            Console.WriteLine(string.Format("BusinessObject:StartProcessing - {0}",dataObject.Add()));
            return "Started";
        }
    }
}
