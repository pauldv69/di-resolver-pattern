﻿namespace DI_Resolver_Business_Layer
{
    public interface IBusinessObject
    {
        string StartProcessing(int id);
    }
}
