﻿using System;
using DI_Resolver;
using DI_Resolver_Business_Layer;
using Ninject;

namespace DI_Resolver_Console
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Starting");

            IKernel kernel = new StandardKernel();

            Resolver.Load(kernel);

            var businessObject = kernel.Get<IBusinessObject>();

            Console.WriteLine(businessObject.StartProcessing(1));

            Console.ReadKey();

        }
    }
}
